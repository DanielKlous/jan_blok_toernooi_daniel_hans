<?php

// pickup the session from the login page like i did below
session_start();

session_unset();
$admin_nav = "";
$admin_nav_location = "";

// we use the if statement here to check if its really the admin thats logged-in
if ($_SESSION['admin'] == "admin") {
    $admin_nav = "ADMIN DASHBOARD";
    $admin_nav_location = "admin_dashboard.php";
} else { // if its not the admin
    $admin_nav = "ADMIN LOGIN";
    $admin_nav_location = "admin_login.php";
}

?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Jan Blok Toernooi</title>
    <meta name="description" content="Dit is de website voor het Jan Blok Toernooi. Hier kunt u speelschema's, wedstrijduitslagen en pouls vinden!">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Oxygen&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/styles.css">
    <?php require "favicon.php" ?>
</head>

<body>
    <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/nl_NL/sdk.js#xfbml=1&version=v6.0"></script>
    <div class="background_container"></div>
    <nav class="nav">
        <ul>
            <li>
                <a href="index.php">HOME</a>
            </li>
            <li>
                <a href="poules.php"><strong>POULES</strong></a>
            </li>
            <li>
                <a href="speelschemas.php">SPEELSCHEMA'S</a>
            </li>
            <li>
                <a href="omroeplijst.php">OMROEPLIJST</a>
            </li>
            <li>
                <a href="uitslagen.php">UITSLAGEN</a>
            </li>
            <li>
                <a href="<?= $admin_nav_location ?>">
                    <?= $admin_nav ?>
                </a>
            </li>
        </ul>
    </nav>
    <div class="container">
        <div class="textbox">
            <div class="introText">
                <img src="content/logo_jan_blok_toernooi.png" alt="Logo Jan Blok Toernooi" class="logo">
            </div>
        </div>
    </div>
    <script src="" async defer></script>
</body>

</html>