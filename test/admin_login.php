<?php

// begin with login system

if(isset($_POST["submit"])) { // selects name attribute of submit button

    // start session, the sesson itself is below
    session_start();

    // select the username and password field (we select the name value here)
    $Username = $_POST['username'];
    $Password = $_POST['password'];

    // session now contains the user-input of username field of the form
    $_SESSION['admin'] = $Username;

    // connect to database
    $servername = "45.13.252.52";
    $username = "u703500310_jan_blok";
    $password = "JanBlokToernooi2020";
    $conn = new mysqli($servername, $username, $password, "u703500310_jan_blok");
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    // MYSQL query to check if form/user data exists in the database, hence a login
    $sql = "SELECT * FROM users where username = '$Username' AND  password = '$Password'";

    // the login first AND a 'session' check, if the session is 'admin' (username field in form), then give it access to a special page xD ;) if not, then go to normal index page
    foreach ($conn->query($sql) as $row) {
        if ($row['username'] == $Username && $row['password'] == $Password) {
            if ($_SESSION['admin'] == "admin") { // admin goes to index.php
                 header('Location: index11.php');
            } else { // user goes to User.php
                 header('Location: User.php');

            }
        }else {
            echo "wrong credentials!";
        }
    }
}

?>


<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Jan Blok Toernooi</title>
    <meta name="description" content="Dit is de website voor het Jan Blok Toernooi. Hier kunt u speelschema's, wedstrijduitslagen en pouls vinden!">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Oxygen&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/styles.css">
    <?php require "favicon.php" ?>
</head>

<body>
    <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/nl_NL/sdk.js#xfbml=1&version=v6.0"></script>
    <div class="background_container"></div>
    <nav class="nav">
        <ul>
            <li>
                <a href="index.php">HOME</a>
            </li>
            <li>
                <a href="poules.php">POULES</a>
            </li>
            <li>
                <a href="speelschemas.php">SPEELSCHEMA'S</a>
            </li>
            <li>
                <a href="omroeplijst.php">OMROEPLIJST</a>
            </li>
            <li>
                <a href="uitslagen.php">UITSLAGEN</a>
            </li>
            <li>
                <a href="admin_login.php"><strong>ADMIN LOGIN</strong></a>
            </li>
        </ul>
    </nav>
    <div class="container">
        <div class="textbox">
            <div class="introText">
                <img src="content/logo_jan_blok_toernooi.png" alt="Logo Jan Blok Toernooi" class="logo">
            </div>
            <div class="login">
    <form action="" method="post">
        <label for="username">
            <i class="fas fa-user"></i>
        </label>
        <input type="text" name="username" placeholder="Username" id="username" required>
        <label for="password">
            <i class="fas fa-lock"></i>
        </label>
        <input type="password" name="password" placeholder="Password" id="password" required>
        <input type="submit" value="Login" name="submit">
    </form>
            </div>
        </div>
    </div>
    <script src="" async defer></script>
</body>
</html>