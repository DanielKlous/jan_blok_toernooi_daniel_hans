<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Login</title>
    <style>

        * {
            box-sizing: border-box;
            font-family: -apple-system, BlinkMacSystemFont, "segoe ui", roboto, oxygen, ubuntu, cantarell, "fira sans", "droid sans", "helvetica neue", Arial, sans-serif;
            font-size: 16px;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
        }
        body {
            background-color: #435165;
        }
        .login {
            width: 400px;
            background-color: #ffffff;
            box-shadow: 0 0 9px 0 rgba(0, 0, 0, 0.3);
            margin: 100px auto;
        }
        .login h1 {
            text-align: center;
            color: #5b6574;
            font-size: 24px;
            padding: 20px 0 20px 0;
            border-bottom: 1px solid #dee0e4;
        }
        .login form {
            display: flex;
            flex-wrap: wrap;
            justify-content: center;
            padding-top: 20px;
        }
        .login form label {
            display: flex;
            justify-content: center;
            align-items: center;
            width: 50px;
            height: 50px;
            background-color: #3274d6;
            color: #ffffff;
        }
        .login form input[type="password"], .login form input[type="text"] {
            width: 310px;
            height: 50px;
            border: 1px solid #dee0e4;
            margin-bottom: 20px;
            padding: 0 15px;
        }
        .login form input[type="submit"] {
            width: 100%;
            padding: 15px;
            margin-top: 20px;
            background-color: #3274d6;
            border: 0;
            cursor: pointer;
            font-weight: bold;
            color: #ffffff;
            transition: background-color 0.2s;
        }
        .login form input[type="submit"]:hover {
            background-color: #2868c7;
            transition: background-color 0.2s;
        }

    </style>
</head>
<body>
<div class="login">
    <h1>Login</h1>
    <form action="" method="post">
        <label for="username">
            <i class="fas fa-user"></i>
        </label>
        <input type="text" name="username" placeholder="Username" id="username" required>
        <label for="password">
            <i class="fas fa-lock"></i>
        </label>
        <input type="password" name="password" placeholder="Password" id="password" required>
        <input type="submit" value="Login" name="submit">
    </form>
</div>
</body>
</html>

<?php

// begin with login system

if(isset($_POST["submit"])) { // selects name attribute of submit button

    // start session, the sesson itself is below
    session_start();

    // select the username and password field (we select the name value here)
    $Username = $_POST['username'];
    $Password = $_POST['password'];

    // session now contains the user-input of username field of the form
    $_SESSION['admin'] = $Username;

    // connect to database
    $servername = "localhost";
    $username = "root";
    $password = "";
    $conn = new mysqli($servername, $username, $password, "forumadminlogin");
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    // MYSQL query to check if form/user data exists in the database, hence a login
    $sql = "SELECT * FROM users where username = '$Username' AND  password = '$Password'";

    // the login first AND a 'session' check, if the session is 'admin' (username field in form), then give it access to a special page xD ;) if not, then go to normal index page
    foreach ($conn->query($sql) as $row) {
        if ($row['username'] == $Username && $row['password'] == $Password) {
            if ($_SESSION['admin'] == "admin") { // admin goes to index.php
                 header('Location: index.php');
            } else { // user goes to User.php
                 header('Location: User.php');

            }
        }else {
            echo "wrong credentials!";
        }
    }
}


?>